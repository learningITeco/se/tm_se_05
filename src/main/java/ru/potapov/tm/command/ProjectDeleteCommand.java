package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.util.Collection;
import java.util.Objects;

public class ProjectDeleteCommand extends AbstractCommand {
    public ProjectDeleteCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "delete-project";
    }

    @Override
    public String getDescription() {
        return "Deletes the project";
    }

    @Override
    public void execute() {
        Project findProject = null;
        boolean circleForProject = true;
        while (circleForProject) {
            System.out.println("Input a project name for removing:");
            String name = getBootstrap().getIn().nextLine();

            if ("exit".equals(name)){
                return;
            }

            findProject = getBootstrap().getProjectService().findProjectByName(name);

            if (Objects.isNull(findProject)) {
                System.out.println("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }

        Collection<Task> listTaskRemoving = getBootstrap().getTaskService().findAll(findProject.getId());
        getBootstrap().getTaskService().removeAll(listTaskRemoving);
        getBootstrap().getProjectService().remove(findProject);
        System.out.println("Completed");
    }
}
