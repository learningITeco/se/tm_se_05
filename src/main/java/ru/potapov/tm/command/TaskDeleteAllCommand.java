package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;

public class TaskDeleteAllCommand extends AbstractCommand {
    public TaskDeleteAllCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "delete-all-tasks";
    }

    @Override
    public String getDescription() {
        return "Deletes all tasks of all projects";
    }

    @Override
    public void execute() {
        getBootstrap().getTaskService().removeAll();
        System.out.println("All tasks of all projects have deleted");
    }
}
