package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;
import ru.potapov.tm.ServiceLocator;

public class HelpCommand extends AbstractCommand {
    public HelpCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Lists all command";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : getBootstrap().getCommands()) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }
}
