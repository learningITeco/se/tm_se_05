package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;

public class ExitCommand extends AbstractCommand {
    public ExitCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Halts the app";
    }

    @Override
    public void execute() {
        System.out.println("Buy-buy....");
    }
}
