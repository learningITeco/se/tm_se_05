package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;
import ru.potapov.tm.entity.Project;

import java.util.Objects;

public class ProjectUpdateCommand extends AbstractCommand {
    public ProjectUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "update-project";
    }

    @Override
    public String getDescription() {
        return "Update name of the Nth project";
    }

    @Override
    public void execute() {
        Project findProject = null;

        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a project name for update:");
            String name = getBootstrap().getIn().nextLine();

            if ("exit".equals(name)){
                return;
            }

            findProject = getBootstrap().getProjectService().findProjectByName(name);

            if (Objects.isNull(findProject)) {
                System.out.println("Project with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
        }

        System.out.println("Do you want to update the name? <y/n>");
        String answer = getBootstrap().getIn().nextLine();
        if ("y".equals(answer) || "Y".equals(answer)){
            System.out.println("Input a new name for this task:");
            try {
                findProject = getBootstrap().getProjectService().renameProject(findProject, getBootstrap().getIn().nextLine());
            }catch (Exception e) {e.printStackTrace();}
            System.out.println("Completed");
        }
    }
}
