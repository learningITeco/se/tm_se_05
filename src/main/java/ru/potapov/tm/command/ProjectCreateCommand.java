package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;
import ru.potapov.tm.entity.Project;

import java.util.Objects;
import java.util.UUID;

public class ProjectCreateCommand extends AbstractCommand {
    public ProjectCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "create-project";
    }

    @Override
    public String getDescription() {
        return "Creates a new project";
    }

    @Override
    public void execute() {
        Project findProject = null;
        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a name for a new project:");
            String name = getBootstrap().getIn().nextLine();

            findProject = getBootstrap().getProjectService().findProjectByName(name);

            if (Objects.nonNull(findProject)) {
                System.out.println("Project with name [" + name + "] already exist");
                continue;
            }

            circleForName = false;
            System.out.println("OK");
            findProject = new Project(name);
        }

        System.out.println("Input a description for this project:");
        findProject.setDescription(getBootstrap().getIn().nextLine());

        //Date:
        findProject.setDateStart(getBootstrap().inputDate("Input a start date for new project in format<dd-mm-yyyy>:"));
        findProject.setDateFinish(getBootstrap().inputDate("Input a finish date for new project:"));

        findProject.setId(UUID.randomUUID().toString());

        getBootstrap().getProjectService().put(findProject);
        System.out.println();
        System.out.println("Project [" + findProject.getName() + "] has created");
        System.out.println("Description: " + findProject.getDescription());
        System.out.println("ID: " + findProject.getId());
        System.out.println("Date start: " + findProject.getDateStart().format(getBootstrap().getFt()));
        System.out.println("Date finish: " + findProject.getDateFinish().format(getBootstrap().getFt()));
    }
}
