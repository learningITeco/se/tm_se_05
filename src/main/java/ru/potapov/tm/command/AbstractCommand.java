package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;

public abstract class AbstractCommand {
    private Bootstrap bootstrap;

    public AbstractCommand(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String getName();
    public abstract String getDescription();
    public abstract void execute();

    public Bootstrap getBootstrap() {
        return bootstrap;
    }
}
