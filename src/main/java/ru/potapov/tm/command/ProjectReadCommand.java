package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;
import ru.potapov.tm.entity.Project;

public class ProjectReadCommand extends AbstractCommand {
    public ProjectReadCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "read-projects";
    }

    @Override
    public String getDescription() {
        return "Reads all projects";
    }

    @Override
    public void execute() {
        System.out.println("All projects: \n");
        if (getBootstrap().getProjectService().checkSize() == 0){
            System.out.println("We have not any project");
            return;
        }

        for (Project project : getBootstrap().getProjectService().getCollectionProjects()) {
            System.out.println();
            System.out.println("Project [" + project.getName() + "]");
            System.out.println("Description: " + project.getDescription());
            System.out.println("ID: " + project.getId());
            System.out.println("Date start: " + project.getDateStart().format(getBootstrap().getFt()));
            System.out.println("Date finish: " + project.getDateFinish().format(getBootstrap().getFt()));
        }
    }
}
