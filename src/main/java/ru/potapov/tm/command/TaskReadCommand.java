package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;
import ru.potapov.tm.entity.Project;

import java.util.Objects;

public class TaskReadCommand extends AbstractCommand {
    public TaskReadCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "read-tasks";
    }

    @Override
    public String getDescription() {
        return "Reads tasks for a current project";
    }

    @Override
    public void execute() {
        if (getBootstrap().getTaskService().checkSize() == 0){
            System.out.println("We have not any project");
            return;
        }

        Project findProject = null;
        boolean circleForProject = true;
        while (circleForProject) {
            System.out.println("Input a project name for reading its tasks:");
            String name = getBootstrap().getIn().nextLine();

            if ("exit".equals(name)){
                return;
            }

            findProject = getBootstrap().getProjectService().findProjectByName(name);

            if (Objects.isNull(findProject)) {
                System.out.println("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }
        getBootstrap().printTasksOfProject(findProject, getBootstrap().getTaskService().findAll(findProject.getId()));
        System.out.println("Completed");
    }
}
