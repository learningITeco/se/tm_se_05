package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;

public class ProjectDeleteAllCommand extends AbstractCommand {
    public ProjectDeleteAllCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "delete-all-projects";
    }

    @Override
    public String getDescription() {
        return "Deletes all project";
    }

    @Override
    public void execute() {
        getBootstrap().getTaskService().removeAll();
        getBootstrap().getProjectService().removeAll();
        System.out.println("Completed");
    }
}
