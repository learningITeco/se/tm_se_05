package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.util.Objects;
import java.util.UUID;

public class TaskCreateCommand extends AbstractCommand {
    public TaskCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "create-task";
    }

    @Override
    public String getDescription() {
        return "Creates a new task";
    }

    @Override
    public void execute() {
        Project findProject = null;
        Task newTask = new Task();


        boolean circleForProject = true;
        while (circleForProject) {
            System.out.println("Input the name of the project for this task:");
            String name = getBootstrap().getIn().nextLine();

            if ("exit".equals(name)){
                return;
            }

            findProject = getBootstrap().getProjectService().findProjectByName(name);

            if (Objects.isNull(findProject)) {
                System.out.println("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
            newTask.setIdProject(findProject.getId());
            System.out.println("OK");
        }


        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a name for a new task:");
            String name = getBootstrap().getIn().nextLine();

            if ("exit".equals(name)){
                return;
            }

            boolean exist = false;

            Task findTask = getBootstrap().getTaskService().findTaskByName(name);


            if (Objects.nonNull(findTask)) {
                System.out.println("Task with name [" + name + "] already exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
            System.out.println("OK");
            newTask.setName(name);
        }

        System.out.println("Input a description for this task:");
        newTask.setDescription(getBootstrap().getIn().nextLine());

        //Date:
        newTask.setDateStart(getBootstrap().inputDate("Input a start date for new project in format<dd-mm-yyyy>:"));
        newTask.setDateFinish(getBootstrap().inputDate("Input a finish date for new project:"));

        newTask.setId(UUID.randomUUID().toString());

        getBootstrap().getTaskService().put(newTask);
        System.out.println();
        System.out.println("Task [" + newTask.getName() + "] has created");
        System.out.println("Project: " + findProject.getName());
        System.out.println("Description: " + newTask.getDescription());
        System.out.println("ID: " + newTask.getId());
        System.out.println("Date start: " + newTask.getDateStart().format(getBootstrap().getFt()));
        System.out.println("Date finish: " + newTask.getDateFinish().format(getBootstrap().getFt()));
    }
}
