package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.util.Collection;
import java.util.Objects;

public class TaskDeleteForProjectCommand extends AbstractCommand {
    public TaskDeleteForProjectCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "delete-tasks";
    }

    @Override
    public String getDescription() {
        return "Deletes all tasks of a project";
    }

    @Override
    public void execute() {
        if (getBootstrap().getTaskService().checkSize() == 0){
            System.out.println("We do not have any task.");
            return;
        }

        Project findProject = null;
        boolean circleForProject = true;
        while (circleForProject) {
            System.out.println("Input a project name for removing its tasks:");
            String name = getBootstrap().getIn().nextLine();

            if ("exit".equals(name)){
                return;
            }

            findProject = getBootstrap().getProjectService().findProjectByName(name);

            if (Objects.isNull(findProject)) {
                System.out.println("Project with name [" + name + "] does not exist, plz try again or type command <exit>");
                continue;
            }

            circleForProject = false;
        }

        Collection<Task> listTaskRemoving = getBootstrap().getTaskService().findAll(findProject.getId());

        getBootstrap().getTaskService().removeAll(listTaskRemoving);
        System.out.println("Completed");
    }
}
