package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;
import ru.potapov.tm.entity.Task;

import java.util.Objects;

public class TaskDeleteCommand extends AbstractCommand {
    public TaskDeleteCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "delete-task";
    }

    @Override
    public String getDescription() {
        return "Deletes a task of a project";
    }

    @Override
    public void execute() {
        if (getBootstrap().getTaskService().checkSize() == 0){
            System.out.println("We do not have any task.");
            return;
        }

        Task findTask = null;

        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a task name for remove:");
            String name = getBootstrap().getIn().nextLine();

            if ("exit".equals(name)){
                return;
            }

            findTask = getBootstrap().getTaskService().findTaskByName(name);

            if (Objects.isNull(findTask)) {
                System.out.println("Task with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
        }

        getBootstrap().getTaskService().remove(findTask);
        System.out.println("The task has deleted");
    }
}
