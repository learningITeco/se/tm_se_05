package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;
import ru.potapov.tm.entity.Project;

public class TaskReadAllCommand extends AbstractCommand {
    public TaskReadAllCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "read-all-tasks";
    }

    @Override
    public String getDescription() {
        return "Reads all tasks";
    }

    @Override
    public void execute() {
        if (getBootstrap().getTaskService().checkSize() == 0) {
            System.out.println("We do not have any tasks");
            return;
        }

        for (Project project : getBootstrap().getProjectService().getCollectionProjects()) {
            getBootstrap().printTasksOfProject(project, getBootstrap().getTaskService().findAll(project.getId()));
        }
        System.out.println("Completed");
    }
}
