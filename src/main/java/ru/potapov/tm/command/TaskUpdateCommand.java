package ru.potapov.tm.command;

import ru.potapov.tm.Bootstrap;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.util.Objects;

public class TaskUpdateCommand extends AbstractCommand {
    private final String YY = "Y";
    private final String Yy = "y";

    public TaskUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "update-task";
    }

    @Override
    public String getDescription() {
        return "Update name or id-projecr of a task";
    }

    @Override
    public void execute() {
        if (getBootstrap().getTaskService().checkSize() == 0){
            System.out.println("We do not have any task.");
            return;
        }

        Task findTask = null;

        boolean circleForName = true;
        while (circleForName){
            System.out.println("Input a task name for update:");
            String name = getBootstrap().getIn().nextLine();

            if ("exit".equals(name)){
                return;
            }

            findTask = getBootstrap().getTaskService().findTaskByName(name);

            if (Objects.isNull(findTask)) {
                System.out.println("Task with name [" + name + "] is not exist, plz try again or type command <exit>");
                continue;
            }

            circleForName = false;
        }

        System.out.println("Do you want to update the name? <y/n>");
        String answer = getBootstrap().getIn().nextLine();
        switch (answer){
            case YY:
            case Yy:
                System.out.println("Input a new name for this task:");
                try {
                    findTask = getBootstrap().getTaskService().renameTask(findTask, getBootstrap().getIn().nextLine());
                    System.out.println("Completed");
                }catch (Exception e){ e.printStackTrace(); }
        }

        System.out.println("Do you want to update the project for this task? <y/n>");
        answer = getBootstrap().getIn().nextLine();
        switch (answer){
            case YY:
            case Yy:
                System.out.println("Input the name of project for this task:");
                Project findProject     = null;
                String name             = getBootstrap().getIn().nextLine();
                try {
                    findProject = getBootstrap().getProjectService().findProjectByName(name);
                }catch (Exception e){ e.printStackTrace();}

                if (Objects.isNull(findProject)){
                    System.out.println("Project with name [" + name + "] is not exist, project for this task does not changed");
                }else {
                    try {
                        getBootstrap().getTaskService().changeProject(findTask, findProject);
                    }catch (Exception e){ e.printStackTrace(); }
                    System.out.println("Completed");
                }
        }
    }
}
