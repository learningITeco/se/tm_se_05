package ru.potapov.tm;

import ru.potapov.tm.service.ServiceProject;
import ru.potapov.tm.service.ServiceTask;

public interface ServiceLocator {
    Bootstrap getTerminalService();
    ServiceProject getProjectService();
    ServiceTask getTaskService();
//    void setServiceLocator(ServiceLocator locator);
//    ServiceLocator getServiceLocator();
}
