package ru.potapov.tm.service;

import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.repository.RepositoryProject;
import ru.potapov.tm.repository.RepositoryTask;

import java.util.*;

public class ServiceTask {
    private RepositoryTask      repositoryTask;
    private RepositoryProject   repositoryProject;

    public ServiceTask() {}

    public ServiceTask(RepositoryTask repositoryTask, RepositoryProject repositoryProject) {
        this.repositoryTask = repositoryTask;
        this.repositoryProject = repositoryProject;
    }

    public RepositoryTask getRepositoryTask() {
        return repositoryTask;
    }

    public int checkSize(){
         return repositoryTask.getCollectionTasks().size();
    }

    public Task findTaskByName(String name){
        Task result = null;

        for (Task task : repositoryTask.getCollectionTasks()) {
            if (task.getName().equals(name)) {
                result = task;
                break;
            }
        }
        return result;
    }

    public void remove(Task task){
        repositoryTask.remove(task);
    }

    public void removeAll(){
        repositoryTask.removeAll();
    }

    public void removeAll(Collection<Task> listTasks){
        repositoryTask.removeAll(listTasks);
    }

    public Task renameTask(Task task, String name) throws CloneNotSupportedException{

        if (Objects.nonNull(name)){
            Task newTask = (Task) task.clone();
            newTask.setName(name);
            return repositoryTask.merge(newTask);
        }
        return task;
    }

    public void changeProject(Task task, Project project) throws CloneNotSupportedException{
        if (Objects.nonNull(project)){
            Task newTask = (Task) task.clone();
            newTask.setIdProject(project.getId());
            repositoryTask.merge(newTask);
        }
    }

    public Collection<Task> findAll(String idProject){
        Collection<Task> listTask = new ArrayList<>();
        for (Task task : repositoryTask.getCollectionTasks()) {
            if (task.getIdProject().equals(idProject)){
                listTask.add(task);
            }
        }
        return listTask;
    }

    public void put(Task task){
        repositoryTask.merge(task);
    }
}
