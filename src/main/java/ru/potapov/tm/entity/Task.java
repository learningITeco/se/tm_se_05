package ru.potapov.tm.entity;

import java.time.LocalDateTime;
import java.util.Date;

public class Task implements Cloneable {
    private String      id;
    private String      name;
    private String      description;
    private LocalDateTime   dateStart;
    private LocalDateTime   dateFinish;
    private String      idProject;

    public Task() {}

    public Task(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getDateStart() {
        return dateStart;
    }

    public LocalDateTime getDateFinish() {
        return dateFinish;
    }

    public String getIdProject() {
        return idProject;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateStart(LocalDateTime dateStart) {
        this.dateStart = dateStart;
    }

    public void setDateFinish(LocalDateTime dateFinish) {
        this.dateFinish = dateFinish;
    }

    public void setIdProject(String idProject) {
        this.idProject = idProject;
    }

    @Override
    public String toString() {
        return "Task ["+getName()+"]";
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
