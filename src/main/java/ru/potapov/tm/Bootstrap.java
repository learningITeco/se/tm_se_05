package ru.potapov.tm;

import ru.potapov.tm.command.*;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.repository.RepositoryProject;
import ru.potapov.tm.repository.RepositoryTask;
import ru.potapov.tm.service.ServiceProject;
import ru.potapov.tm.service.ServiceTask;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Bootstrap implements ServiceLocator {
    private RepositoryProject            repositoryProject;
    private RepositoryTask               repositoryTask;
    private ServiceProject               serviceProject;
    private ServiceTask                  serviceTask;
    private Map<String, AbstractCommand> mapCommand;

    private Project             currentProject;
    private DateTimeFormatter   ft;
    private Scanner             in;

    //Constants
    private final String YY = "Y";
    private final String Yy = "y";

    public Bootstrap() {
        repositoryProject   = new RepositoryProject();
        repositoryTask      = new RepositoryTask();
        serviceProject      = new ServiceProject(repositoryProject);
        serviceTask         = new ServiceTask(repositoryTask, repositoryProject);
        mapCommand          = new LinkedHashMap<>();

        currentProject      = null;
        ft                  = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    }

    public void init(){
        in = new Scanner(System.in);

        regestry(new ExitCommand(this));
        regestry(new HelpCommand(this));
        regestry(new ProjectCreateCommand(this));
        regestry(new ProjectReadCommand(this));
        regestry(new ProjectUpdateCommand(this));
        regestry(new ProjectDeleteCommand(this));
        regestry(new ProjectDeleteAllCommand(this));
        regestry(new TaskCreateCommand(this));
        regestry(new TaskReadAllCommand(this));
        regestry(new TaskReadCommand(this));
        regestry(new TaskUpdateCommand(this));
        regestry(new TaskDeleteCommand(this));
        regestry(new TaskDeleteForProjectCommand(this));
        regestry(new TaskDeleteAllCommand(this));

        try {
            start();
        }catch (Exception e){
            System.out.println("Something went wrong...");
            e.printStackTrace();
        }
    }

    private void start() throws Exception{
        System.out.println("*** WELCOME TO TASK MANAGER! ***");
        String command = "";
        while (!"exit".equals(command)){
            System.out.println("\nInsert your command in low case or command <help>:");
            command = in.nextLine();
            execute(command);
        }
    }

    private void execute(String command) throws Exception{
        if ( Objects.isNull(command) || command.isEmpty())
            return;

        AbstractCommand abstractCommand = mapCommand.get(command);

        if (Objects.isNull(abstractCommand))
            return;

        abstractCommand.execute();
    }

    private void regestry(AbstractCommand command){
        if ( Objects.isNull(command))
            return;

        final String commandName        = command.getName();
        final String commandDescription = command.getDescription();

        if (Objects.isNull(commandName) || Objects.isNull(commandDescription)
            || commandName.isEmpty() || commandDescription.isEmpty())
            return;

        mapCommand.put(commandName, command);
    }

    public void printTasksOfProject(Project project, Collection<Task> listTasks){
        System.out.println();
        System.out.println("Project [" + project.getName() + "]:");
        for (Task task : listTasks) {
            if (task.getIdProject().equals(project.getId())){
                System.out.println();
                System.out.println("    Task [" + task.getName() + "]");
                System.out.println("    Description: " + task.getDescription());
                System.out.println("    ID: " + task.getId());
                System.out.println("    Date start: " + task.getDateStart().format(ft));
                System.out.println("    Date finish: " + task.getDateFinish().format(ft));
            }
        }
    }

    public LocalDateTime inputDate(String massage){
        System.out.println(massage);
        String strDate = in.nextLine();
        LocalDateTime date;
        try {
            int year = Integer.parseInt(strDate.substring(6)),
                    month= Integer.parseInt(strDate.substring(3,5)),
                    day  = Integer.parseInt(strDate.substring(0,2));
            date = LocalDateTime.of(year,month, day,0,0);
        }catch (Exception e){
            System.out.println("Error formate date! Date set to the end of year.");
            date = LocalDateTime.of(LocalDateTime.now().getYear(),12,31,23,59);
        }

        return date;
    }

    public void printNotCorrectCommand() {
        System.out.println("not correct command, plz try again or type command <help> \n");
    }

    private void printNotChoosenCurrentProject() {
        System.out.println("There isn't any choosen projects! Plz, choose a project!");
    }

    private void commandExit() {
        System.out.println("Buy-buy....");
    }

    public ServiceProject getProjectService() {
        return serviceProject;
    }

    public ServiceTask getTaskService() {
        return serviceTask;
    }

    @Override
    public Bootstrap getTerminalService() {
        return this;
    }

    public Scanner getIn() {
        return in;
    }

    public DateTimeFormatter getFt() {
        return ft;
    }

    public Collection<AbstractCommand> getCommands(){
        return mapCommand.values();
    }
}
