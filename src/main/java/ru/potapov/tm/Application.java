package ru.potapov.tm;

import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Application ***
 * v 1.0.3
 */
public class Application {
    Bootstrap               bootstrap;

    public Application() {
        bootstrap       = new Bootstrap();
    }

    public static void main(String[] args) {
        new Application().go();
    }

    public void go() {
        bootstrap.init();
    }
}
